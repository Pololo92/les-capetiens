

// horloge

var clock = document.getElementById("clock");
var hexColor = document.getElementById("hex-color");

function hexClock() {
  var time = new Date();
  var hours = time.getHours().toString();
  var minutes = time.getMinutes().toString();
  var seconds = time.getSeconds().toString();

  var hoursRandom = Math.ceil(Math.random() * 1);
  console.log(hoursRandom);
  var minutesRandom = Math.ceil(Math.random() * 30);
  var secondsRandom = Math.ceil(Math.random() * 9);

  if (hours.length < 2) {
    hours = "0" + hours; // en cas de mono digit
  }

  if (minutes.length < 2) {
    minutes = "0" + minutes;
  }

  if (seconds.length < 2) {
    seconds = "0" + seconds;
  }

  var clockStr = hours + " : " + minutes + " : " + seconds; // affichage de l'horloge
  var hexColorStr = "#" + hoursRandom + minutesRandom + secondsRandom; // affichage du code hex par rapport a l'heure

  clock.textContent = clockStr; // applique la variable d'affichage
 //applique la variable d'affichage

  if (time.getSeconds() % 2) {
    hexColor.textContent = hexColorStr;
    document.getElementById("clock").style.color = hexColorStr; // changement du bg de l'horloge avec le code hex chaque seconde
  }
}

hexClock();
setInterval(hexClock, 1000); // 1sec





/* Slider Manuel */

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
} 


//Slide Automatique

var i = 0;
var images = [];
var time = 2500;

//Liste d'images

images[0] = 'img/diapo0.jpg';
images[1] = 'img/diapo1.jpg';
images[2] = 'img/diapo2.jpg';
images[3] = 'img/diapo3.jpg';
images[4] = 'img/diapo4.jpg';

//Changer l'image

function changeImg(){
  document.slide.src = images[i];
  
  if(i < images.length - 1){
    i++;
  } else {
    i = 0;
  }

  setTimeout("changeImg()", time);
}


window.onload = changeImg;


/* Lien Actif */

// Get the container element
var btnContainer = document.getElementById("menu");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("nav-links");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace("active", "");
    this.className += "active";
  });
}