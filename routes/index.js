var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('Accueil');
});

router.get('/Huguescapet', function(req, res, next) {
  res.render('Huguescapet');
});

router.get('/PhilippeAuguste', function(req, res, next) {
  res.render('Philippeauguste');
});

router.get('/SaintLouis', function(req, res, next) {
  res.render('Saintlouis');
});

router.get('/Lebelphilippe', function(req, res, next) {
  res.render('Lebelphilippe');
});

router.get('/Formulaire', function(req, res, next) {
  res.render('Formulaire');
});


module.exports = router;
